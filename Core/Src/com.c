#include <stddef.h>
#include "com.h"
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#define SPEED_MULTIPLIER 1e-3

static comReadStatus_t parseSync(const uint8_t *begin, const uint8_t **end);

static comReadStatus_t parsePayload(const uint8_t *begin, const uint8_t *end, comWheelSpeed_t *command);

static bool addSpeed(uint8_t *buffer, size_t size, double speed);

comReadStatus_t comReadCommand(const uint8_t *buffer, size_t size, comWheelSpeed_t *command) {
    const uint8_t *begin = buffer;
    const uint8_t *end = buffer + size;

    bool ret = true;
    comReadStatus_t retStatus = parseSync(begin, &end);
    ret = retStatus == Success;

    if (ret) {
        retStatus = parsePayload(begin, end, command);
    }

    return retStatus;
}

comReadStatus_t parseSync(const uint8_t *begin, const uint8_t **end) {
    for (const uint8_t *it = begin; it < *end; ++it) {
        if (*it == '\n') {
            *end = it;
            return Success;
        }
    }
    return NeedMore;
}

comReadStatus_t parsePayload(const uint8_t *begin, const uint8_t *end, comWheelSpeed_t *command) {
    const char *numBegin = (char*)begin;
    char* pEnd;

    for (int i =0; i < 3; ++i) {
        errno = 0;
        long out = strtol(numBegin, &pEnd, 10);
        if (numBegin == pEnd) {
            return InvalidPayload;
        }
        if (*pEnd != ',') {
            return SyntaxError;
        }
        if (errno == ERANGE) {
            return InvalidPayload;
        }
        numBegin = pEnd + 1;
        command->speed[i] = (double) out * SPEED_MULTIPLIER;
    }
    long out = strtol(numBegin, &pEnd, 10);
    if (numBegin == pEnd) {
        return SyntaxError;
    }
    if (errno == ERANGE) {
        return InvalidPayload;
    }
    if (*pEnd == ',') {
      return InvalidPayload;
    }
    command->speed[3] = (double) out * SPEED_MULTIPLIER;

    return Success;
}

size_t comSerialize(uint8_t *buffer, size_t size, const comWheelSpeed_t *speeds) {
    uint8_t *numberBegin = buffer;
    size_t sizeLeft = size;
    bool ret = true;

    for (int i = 0; i < 4; ++i) {
        ret = addSpeed(numberBegin, sizeLeft, speeds->speed[i]);
        if (ret) {
            for (; *numberBegin != '\0'; --sizeLeft, ++numberBegin) {
                if (sizeLeft == 0u) {
                    ret = false;
                    break;
                }
            }
        }
        if (!ret) {
            break;
        }
        *numberBegin = ',';
        ++numberBegin;
    }
    *(--numberBegin) = '\n';

    return numberBegin - buffer + 1;
}

static bool addSpeed(uint8_t *buffer, size_t size, double speed) {
    bool ret = size >= 6;

    if (ret) {
        int intSpeed = (int) (speed / SPEED_MULTIPLIER);
        snprintf((char *) buffer, 6, "%d", intSpeed);
    }
    return ret;
}