#include "pid.h"
#include "err.h"

double pidCalculateOutput(double setPoint, double currentValue, double dt, pidPID_t *pidController) {
    double ret = 0;
    const double error = setPoint - currentValue;

    ret += pidController->p * error;
    if (pidController->d != 0.) {
        ret += pidDifferentiate(error, dt, pidController);
    }
    if (pidController->i != 0.) {
        ret += pidIntegrate(error, dt, pidController);
    }
    return ret;
}

double pidIntegrate(double error, double dt, pidPID_t *pid) {
    pid->iAcc += error * dt * pid->i;
    pidClamp(&(pid->iAcc), &pid->clamp);

    return pid->iAcc;
}

double pidDifferentiate(double error, double dt, pidPID_t *pid) {
    double ret;
    ret = pid->d * (error - pid->dLast) / dt;
    pid->dLast = error;
    return ret;
}

void pidClamp(double *value, const pidClampConf_t *clampConf) {
    if (!clampConf->clamped) {
        return;
    }
    if (clampConf->clampMin >= clampConf->clampMax) {
        errHalt(PID_ERROR_CODE, PID_ERROR_CLAMPING);
        return;
    }
    if (*value > clampConf->clampMax) {
        *value = clampConf->clampMax;
    } else if (*value < clampConf->clampMin) {
        *value = clampConf->clampMin;
    }
}

void pidClearPID_State(pidPID_t *pidController) {
    pidController->iAcc = 0.;
    pidController->dLast = 0.;
}