/** 
 * encr module implementation
 */

#include "encr.h"
#include "err.h"
#include "math.h"
#include <stdint.h>

static double intPositionResolution = 0.;

// region PRIVATE_DEFINES ###############################

#define Q1 1u
#define Q2 2u
#define Q3 3u
#define Q4 4u
#define MAX_UINT16 0xFFFFu
#define MAX_UINT16_3_4 0xC000u
#define MAX_UINT16_1_2 0x8000u
#define MAX_UINT16_1_4 0x4000u

// endregion ############################################

uint8_t getQuarter(const uint32_t value) {
    uint8_t ret;

    if (value >= 0u && value < MAX_UINT16_1_4) {
        ret = Q1;
    } else if (value >= MAX_UINT16_1_4 && value < MAX_UINT16_1_2) {
        ret = Q2;
    } else if (value >= MAX_UINT16_1_2 && value < MAX_UINT16_3_4) {
        ret = Q3;
    } else { // value >= MAX_UINT16_3_4 && val < MAX_UINT16
        ret = Q4;
    }

    return ret;
}

/**
 * Difference (currentPos - lastPos) concerning int overflow
 *
 * @remarks
 * currentPos = 0xFFFF
 * lastPos = 0x1
 * return -2
 */
int32_t getRawPositionDelta(const uint32_t currentPos, const uint32_t lastPos) {
    int32_t diff;
    uint8_t lastQ = getQuarter(lastPos);
    uint8_t currentQ = getQuarter(currentPos);

    if (lastQ == Q4 && currentQ == Q1) {
        diff = (int32_t) ((MAX_UINT16 - lastPos) + currentPos) + 1;
    } else if (lastQ == Q1 && currentQ == Q4) {
        diff = (int32_t) -((MAX_UINT16 - currentPos) + lastPos) - 1;
    } else {
        diff = (int32_t) (currentPos - lastPos);
    }

    return diff;
}

void encrInit(uint32_t ticksPerRev) {
    if(ticksPerRev == 0){
        errHalt(ENCR_ERROR_CODE, ENCR_ERROR_ZERO_TICKS_PER_REV);
        return;
    }
    intPositionResolution =  2. * M_PI / (double) ticksPerRev;
}

double encrGetPositionResolution() {
    return intPositionResolution;
}

double encrCalculateVelocity(double dt, encrData_t *encoderData) {
    double retVelocity;

    if (intPositionResolution <= 0) {
        errHalt(ENCR_ERROR_CODE, ENCR_ERROR_MODULE_NOT_INITIALIZED);
        return 0;
    }

    uint32_t currentPos = *(encoderData->pCurrentPosition);
    int rawPositionDelta = getRawPositionDelta(currentPos, encoderData->_lastPosition);
    double posDelta = (double) rawPositionDelta * intPositionResolution;
    retVelocity = posDelta / dt;
    encoderData->_lastPosition = currentPos;
    encoderData->currentVelocity = retVelocity;
    return retVelocity;
}