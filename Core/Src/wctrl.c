#include "wctrl.h"
#include "err.h"

double wctrlGetCurrentVelocity(const WheelController_t *wheelController) {
    return wheelController->encoderData.currentVelocity;
}

double wctrlGetTarget(const WheelController_t *wheelController) {
    return wheelController->target;
}

void wctrlSetTarget(double target, WheelController_t *wheelController) {
    if (target == 0.) {
        wctrlStop(wheelController);
    } else {
        wheelController->target = target;
        if (target > 0.) {
            mtpwmSetDirection(&wheelController->motorController, Forward);
            // todo: decide if pid shall be cleared
        } else {
            mtpwmSetDirection(&wheelController->motorController, Backward);
        }
    }
}

void wctrlStep(double dt, WheelController_t *wheelController) {
    encrCalculateVelocity(dt, &wheelController->encoderData);
    double directionMultiplier;
    switch (wheelController->motorController.currentDirection) {
        case Forward:
            directionMultiplier = 1.;
            break;
        case Backward:
            directionMultiplier = -1.;
            break;
        default:
            errHalt(WCTRL_ERROR_CODE, WCTRL_ERROR_STEP_DIRECTION_NONE);
            return;
    }
    double effort;
    if (wheelController->closedLoop) {
        double absoluteTarget = wheelController->target * directionMultiplier;
        double absoluteCurrent = wheelController->encoderData.currentVelocity * directionMultiplier;
        effort = pidCalculateOutput(absoluteTarget,
                                    absoluteCurrent,
                                    dt,
                                    &wheelController->pidController);

    } else {
        effort = wheelController->target * directionMultiplier;
    }
    mtpwmSetEffort(&wheelController->motorController, effort);
    wheelController->currentEffort = effort * directionMultiplier; // invert if needed
}

void wctrlStop(WheelController_t *wheelController) {
    wheelController->target = 0;
    pidClearPID_State(&wheelController->pidController);
    mtpwmStop(&wheelController->motorController, HardStop);
    wheelController->currentEffort = 0;
}

void wctrlSetClosedLoop(bool enable, WheelController_t *wheelController) {
    wheelController->closedLoop = enable;
    if (!enable) {
        pidClearPID_State(&wheelController->pidController);
    }
}