#include "cmsis_os.h"

#include "wctrl.h"
#include "semphr.h"
#include "stdio.h"
#include "string.h"
#include "control_config.h"
#include "com.h"
#include "main.h"

#define CONCAT(e1, e2) e1 ## e2
#define WHEEL_CONTROLLER(wheel) {                                                           \
        .motorController = {                                                                \
        .portA = { .port = CONCAT(wheel, _O_A_GPIO_Port), .pin = CONCAT(wheel, _O_A_Pin) }, \
        .portB = { .port = CONCAT(wheel, _O_B_GPIO_Port), .pin = CONCAT(wheel, _O_B_Pin) }, \
        .wheelPWM = CONCAT(wheel, _PWM),                                                    \
},                                                                                          \
        .encoderData = { .pCurrentPosition = CONCAT(wheel, _ENC_TICK) },                    \
        .pidController = {                                                                  \
            .p = PID_P,                                                                     \
            .i = PID_I,                                                                     \
            .d = PID_D,                                                                     \
            .clamp = {.clamped = PID_CLAMPED,                                               \
                      .clampMax = PID_CLAMP_MAX,                                            \
                      .clampMin = PID_CLAMP_MIN} },                                         \
        .closedLoop = PID_ENABLE                                                            \
}

#define STOP_REQUESTED (1u<<0u)
#define RECEIVED_MSG (1u<<1u)

extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim5;
extern TIM_HandleTypeDef htim10;
extern TIM_HandleTypeDef htim11;

extern UART_HandleTypeDef huart2;

extern osSemaphoreId_t userActionRequestHandle;
extern osMutexId_t wheelsMutexHandle;
extern osSemaphoreId_t uartTxSemaphoreHandle;
extern osSemaphoreId_t userActiveHandle;

extern osEventFlagsId_t controlEventsHandle;

WheelController_t wheelControllers[] = {
        WHEEL_CONTROLLER(W1),
        WHEEL_CONTROLLER(W2),
        WHEEL_CONTROLLER(W3),
        WHEEL_CONTROLLER(W4)
};

mtpwmPort_t standbyPort = {DRV_NSTDBY_GPIO_Port, DRV_NSTDBY_Pin};

comBuffer_t comBuffer;

static void initHAL_Timers(void);

void WheelControlLoop(void *argument) {

    const unsigned periodMs = CONTROL_LOOP_PERIOD_MS;
    const double periodSec = periodMs / 1000.;

    initHAL_Timers(); // pwm and encoders
    encrInit(ENCR_TICKS_PER_REVOLUTION);
    mtpwmSetStandbyPort(standbyPort);
    mtpwmSetMode(PowerOn);

    TickType_t lastWake = xTaskGetTickCount();
    for (;;) {

        if (osMutexAcquire(wheelsMutexHandle, pdMS_TO_TICKS(3 * periodMs / 4)) == osOK) {
            if ((osEventFlagsGet(controlEventsHandle) & STOP_REQUESTED) == 1) {
                for (int i = 0; i < NUMBER_OF_WHEELS; ++i) {
                    wctrlStop(&wheelControllers[i]);
                }
                osEventFlagsClear(controlEventsHandle, STOP_REQUESTED);
            }
            for (int i = 0; i < NUMBER_OF_WHEELS; ++i) {
                wctrlStep(periodSec, &wheelControllers[i]);
            }
            osMutexRelease(wheelsMutexHandle);
        }
        vTaskDelayUntil(&lastWake, pdMS_TO_TICKS(periodMs));
    }

}

void DiodeBlink(void *argument) {

    /* Infinite loop */
    for (;;) {
        HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
        osDelay(pdMS_TO_TICKS(DIODE_BLINK_PERIOD_MS)); // 5 Hz
    }

}

void SerialWrite(void *argument) {

    uint8_t msg[COM_BUFFER_SIZE];
    comWheelSpeed_t wheelSpeeds;

    for (;;) {
        if (osMutexAcquire(wheelsMutexHandle, 50) == osOK) {
            for (int i = 0; i < NUMBER_OF_WHEELS; ++i) {
                wheelSpeeds.speed[i] = wctrlGetCurrentVelocity(&wheelControllers[i]);
            }
            osMutexRelease(wheelsMutexHandle);
            if (osSemaphoreAcquire(uartTxSemaphoreHandle, pdMS_TO_TICKS(SERIAL_COM_LOOP_PERIOD_MS / 2)) == osOK) {
                size_t n = comSerialize(msg, COM_BUFFER_SIZE, &wheelSpeeds);

                if (n > 0) {
                    HAL_UART_Transmit_DMA(&huart2, msg, n);
                }
            }
        }
        osDelay(pdMS_TO_TICKS(SERIAL_COM_LOOP_PERIOD_MS));
    }

}

void SerialHandle(void *argument) {
    comWheelSpeed_t wheelSpeeds;

    for (;;) {
        uint32_t status = osEventFlagsWait(controlEventsHandle, RECEIVED_MSG, osFlagsWaitAll, osWaitForever);
        if ((status & osFlagsError) == 0) {
            if (comReadCommand(comBuffer.process.buffer, comBuffer.process.count, &wheelSpeeds) == Success) {
                if(osMutexAcquire(wheelsMutexHandle, 50) == osOK){
                    osSemaphoreRelease(userActiveHandle);
                    for(int i=0; i< NUMBER_OF_WHEELS; ++i){
                        wctrlSetTarget(wheelSpeeds.speed[i], &wheelControllers[i]);
                    }
                    osMutexRelease(wheelsMutexHandle);
                }
            }
        }
    }
}

void CheckUserActivityCallback(void *argument) {
  if (osSemaphoreGetCount(userActiveHandle) > 0) {
      osSemaphoreAcquire(userActiveHandle, 0);
  } else {
      osEventFlagsSet(controlEventsHandle, STOP_REQUESTED);
  }
}

HAL_StatusTypeDef startUART_receive() {
    return HAL_UARTEx_ReceiveToIdle_DMA(&huart2, comBuffer.receive, COM_BUFFER_SIZE);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
    if (GPIO_Pin == B1_Pin) {
        osSemaphoreRelease(userActionRequestHandle);
    }
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t size) {
    if (huart->Instance == USART2) {
        if ((osEventFlagsGet(controlEventsHandle) & RECEIVED_MSG) == 0) {
            memcpy(comBuffer.process.buffer, comBuffer.receive, size);
            comBuffer.process.count = size;
            osEventFlagsSet(controlEventsHandle, RECEIVED_MSG);
        }

        startUART_receive();
    }
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {
    if (huart->Instance == USART2) {
        osSemaphoreRelease(uartTxSemaphoreHandle);
    }
}

void HandleRequests(void *argument) {
    static const double resolution = 3.;
    static const double maxVel = 33.;
    static int increment = 0;
    static double velocity = 0.;

    for (;;) {
        if (osSemaphoreAcquire(userActionRequestHandle, osWaitForever) == osOK) {
            osSemaphoreRelease(userActiveHandle);
            if (increment) {
                velocity += resolution;
                if (velocity > maxVel) {
                    velocity -= 2 * resolution;
                    increment = 0;
                }
            } else {
                velocity -= resolution;
                if (velocity < -maxVel) {
                    velocity += 2 * resolution;
                    increment = 1;
                }
            }
            for (int i = 0; i < NUMBER_OF_WHEELS; ++i) {
                wctrlSetTarget(-velocity, &wheelControllers[i]);
            }
        }

        osThreadYield();
    }
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart) {
    if ((huart->Instance == USART2) && ((huart->Instance->CR1 & USART_CR1_IDLEIE_Msk) == 0)/* IDLE interrupt disabled */ ) {
        startUART_receive(); // enable again
    }
}

void initHAL_Timers(void) {
    HAL_TIM_PWM_Start(&htim11, TIM_CHANNEL_1);
    HAL_TIM_Encoder_Start(&htim1, TIM_CHANNEL_ALL);

    HAL_TIM_PWM_Start(&htim10, TIM_CHANNEL_1);
    HAL_TIM_Encoder_Start(&htim4, TIM_CHANNEL_ALL);

    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
    HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);

    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
    HAL_TIM_Encoder_Start(&htim5, TIM_CHANNEL_ALL);

}