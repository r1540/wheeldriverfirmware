#include <string.h>
#include "mtpwm.h"
#include "err.h"
#include "main.h"

// region PRIVATE DEFINES ##################################

#define PWM_MIN 0
#define PWM_MAX PWM_TIMERS_ARR

// endregion ###############################################

// region PRIVATE VARIABLES

static mtpwmMode_t mtpwmCurrentMode = NotInitialized;
static mtpwmPort_t mtpwmStandbyPort = {0};

// endregion ###############################################

// region PRIVATE FUNCTIONS PROTOTYPES #####################

static inline void setPortState(mtpwmPort_t port, GPIO_PinState state);

static inline uint32_t mapEffortToPWM(double effort);

// endregion ###############################################

// region PUBLIC FUNCTIONS PROTOTYPES ######################

void mtpwmSetEffort(mtpwmMotorControl_t *motor, double effort) {
    if(mtpwmCurrentMode != PowerOn){
        errHalt(MTPWM_ERROR_CODE, MTPWM_ERROR_OPERATING_DURING_STANDBY);
    }

    if(motor->currentDirection == None){
        errHalt(MTPWM_ERROR_CODE,MTPWM_ERROR_DIRECTION_NOT_SET);
        return;
    }

    if(effort < EFFORT_MIN){
        effort = EFFORT_MIN;
    }else if(effort > EFFORT_MAX){
        effort = EFFORT_MAX;
    }

    *motor->wheelPWM = mapEffortToPWM(effort);
}

void mtpwmStop(mtpwmMotorControl_t *motor, mtpwmStopType_t stopType) {
    GPIO_PinState aState, bState;

    *motor->wheelPWM = 0u;
    motor->currentDirection = None;

    switch (stopType) {
        case HardStop:
            aState = GPIO_PIN_SET;
            bState = GPIO_PIN_SET;
            break;
        case SoftStop:
            aState = GPIO_PIN_RESET;
            bState = GPIO_PIN_RESET;
            break;
        default:
            errHalt(MTPWM_ERROR_CODE, MTPWM_ERROR_UNKNOWN_STOP_TYPE);
            return;
    }
    setPortState(motor->portA, aState);
    setPortState(motor->portB, bState);
}

void mtpwmSetDirection(mtpwmMotorControl_t *motor, mtpwmDirection_t direction) {
    if(mtpwmCurrentMode != PowerOn){
        errHalt(MTPWM_ERROR_CODE, MTPWM_ERROR_OPERATING_DURING_STANDBY);
    }

    GPIO_PinState aState, bState;
    switch (direction) {
        case Forward:
            aState = GPIO_PIN_SET;
            bState = GPIO_PIN_RESET;
            break;
        case Backward:
            aState = GPIO_PIN_RESET;
            bState = GPIO_PIN_SET;
            break;
        default:
            errHalt(MTPWM_ERROR_CODE, MTPWM_ERROR_UNKNOWN_DIRECTION);
            return;
    }
    setPortState(motor->portA, aState);
    setPortState(motor->portB, bState);
    motor->currentDirection = direction;
}

void mtpwmSetMode(const mtpwmMode_t mode) {
    if(mtpwmCurrentMode == NotInitialized){
        errHalt(MTPWM_ERROR_CODE, MTPWM_ERROR_STANDBY_PIN_NOT_SET);
    }

    switch (mode) {
        case Standby:
            setPortState(mtpwmStandbyPort, GPIO_PIN_RESET);
            break;
        case PowerOn:
            setPortState(mtpwmStandbyPort, GPIO_PIN_SET);
            break;
        default:
            errHalt(MTPWM_ERROR_CODE, MTPWM_ERROR_UNKNOWN_MODE);
            break;
    }
    mtpwmCurrentMode = mode;
}

mtpwmMode_t mtpwmGetMode() {
    return mtpwmCurrentMode;
}

void mtpwmSetStandbyPort(mtpwmPort_t port) {
    memcpy((void*)&mtpwmStandbyPort, (void*)&port, sizeof(mtpwmPort_t));
    mtpwmCurrentMode = Standby;
}

// endregion ###############################################

// region PRIVATE FUNCTIONS IMPLEMENTATIONS ################

inline void setPortState(mtpwmPort_t port, GPIO_PinState state) {
    HAL_GPIO_WritePin(port.port, port.pin, state);
}

inline uint32_t mapEffortToPWM(double effort) {
    return (uint32_t) ((effort - EFFORT_MIN) / (EFFORT_MAX - EFFORT_MIN) * (double) (PWM_MAX - PWM_MIN)) + PWM_MIN;
}

// endregion ###############################################