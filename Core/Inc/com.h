/** 
 * Communication module responsible for (de)serializing data from serial port.
 */

#ifndef WHEELDRIVERFIRMWARE_COM_H
#define WHEELDRIVERFIRMWARE_COM_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"{
#endif

#define COM_BUFFER_SIZE 50

/// Com reading status enum
typedef enum {
    Success,        ///< Message read and parsed successfully
    NeedMore,       ///< More message content is expected
    InvalidPayload, ///< Wrong payload content
    SyntaxError,    ///< Message syntax error
    Failure         ///< Internal parsing error
} comReadStatus_t;

/// Helper struct for wheel speed reading/serializing
typedef struct {
    double speed[4]; ///< Array of speeds. Order is remained.
} comWheelSpeed_t;

/// Com processing buffer
typedef struct {
    uint8_t buffer[COM_BUFFER_SIZE]; ///< Receive buffer
    size_t count; ///< Number of bytes tobe processed
} comProcessBuffer_t;

/// Structure containing buffers useful for reading/serializing
typedef struct {
    uint8_t receive[COM_BUFFER_SIZE];   ///< Raw receive buffer. Here should be written incoming message (e.g. via DMA)
    comProcessBuffer_t process;         ///< Processing structure. Here should be written message ready for parsing
    uint8_t transmit[COM_BUFFER_SIZE];  ///< Output buffer. Place output bytes here.
} comBuffer_t;

/**
 * Parse incoming message
 * @param buffer Buffer with message content
 * @param size Size of message
 * @param[out] command Target structure containing deserialized content
 * @return Parsing status
 */
comReadStatus_t comReadCommand(const uint8_t *buffer, size_t size, comWheelSpeed_t *command);

/**
 * Serialize message
 * @param[out] buffer Output buffer
 * @param size Maximal capacity of buffer
 * @param speeds Content to be serialized
 * @return Number of bytes written to buffer. 0 - in case of serializing error.
 * @details Possible error cases:
 *  - buffer max size is too small for message content
 */
size_t comSerialize(uint8_t *buffer, size_t size, const comWheelSpeed_t *speeds);

#ifdef __cplusplus
};
#endif

#endif //WHEELDRIVERFIRMWARE_COM_H
