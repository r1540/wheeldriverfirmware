/// PID controller utilities

#ifndef WHEELDRIVERFIRMWARE_PID_H
#define WHEELDRIVERFIRMWARE_PID_H

#include <stdint.h>
#include <stdbool.h>

/// Module error code
#define PID_ERROR_CODE 1U

/// Clamping error code
#define PID_ERROR_CLAMPING 1U

#ifdef __cplusplus
extern "C"{
#endif

/**
 * Contains information about clamping values
 */
typedef struct {
    bool clamped;      ///< indicates if value should be clamped
    double clampMax;   ///< clamping max cap
    double clampMin;   ///< clamping min cap
} pidClampConf_t;

/**
 * Stores current PID controller state
 */
typedef struct {
    double p;         ///< P coefficient, in order to turn off set to 0
    double i;         ///< I coefficient in order to turn off set to 0
    double d;         ///< D coefficient in order to turn off set to 0

    double iAcc;     ///< Value accumulated by integration part
    double dLast;    ///< Las input value used by differential part

    pidClampConf_t clamp; ///< Clamping configuration applied to pidPID_t.iAcc
} pidPID_t;

/**
 * Calculates PID output and updates its state
 * @param setPoint Setpoint of process
 * @param currentValue Current process value
 * @param dt Time step
 * @param pidController PID struct
 * @return Value calculated basing on current PID state and input values
 *
 * @remarks
 * This function implements parallel PID controller algorithm.
 * For more info see https://instrumentationtools.com/pid-controllers/
 */
double pidCalculateOutput(double setPoint, double currentValue, double dt, pidPID_t *pidController);

/**
 * Perform integration of PID pid and update its internal state
 * @param error Integrator input value (whole PID inpute at the same time)
 * @param dt Time step
 * @param pid Integrator struct
 * @return Latest accumulated value
 */
double pidIntegrate(double error, double dt, pidPID_t *pid);

/**
 * Perform differentiation of PID derivative part and update its internal state
 * @param error Differentiator input value (whole PID inpute at the same time)
 * @param dt Time step
 * @param differentiator
 * @return Output value
 */
double pidDifferentiate(double error, double dt, pidPID_t *pid);

/**
 * Clamp value basing on clamp configuration
 * @param[in,out] value Value to be clamped
 * @param clampConf Clamp configuration struct
 */
void pidClamp(double *value, const pidClampConf_t *clampConf);

/**
 * Clears PID internal state (pidPID_t.iAcc and pidPID_t.dLast)
 * @param pidController pid controller instance
 */
void pidClearPID_State(pidPID_t *pidController);

#ifdef __cplusplus
};
#endif

#endif //WHEELDRIVERFIRMWARE_PID_H
