/** 
 * Module responsible for reading and processing incremental encoder data.
 * Module has to be initialized by @ref encrInit function before use.
 */

#ifndef WHEELDRIVERFIRMWARE_ENCR_H
#define WHEELDRIVERFIRMWARE_ENCR_H

#include <stdint.h>

// region PUBLIC DEFINES ##################################

/// errHalt module code
#define ENCR_ERROR_CODE 3u

/// Raised in case of using not initialized module (or initialized wrongly)
#define ENCR_ERROR_MODULE_NOT_INITIALIZED 1u

/// Raised in case of call encrInit with argument equal 0
#define ENCR_ERROR_ZERO_TICKS_PER_REV 2u

// endregion ##############################################

#ifdef __cplusplus
extern "C" {
#endif

/// Structure containing encoder data processing variables.
typedef struct {
    double currentVelocity;                 ///< The latest calculated velocity data [rad/s]
    volatile uint32_t* const pCurrentPosition;   ///< In case of using DMA, pointer to current encoder position
    uint32_t _lastPosition;                      ///< [private] position recorded during last velocity calculation
}encrData_t;

/**
 * Initialize module
 * @param ticksPerRev encoder ticks per wheel revolution
 */
void encrInit(uint32_t ticksPerRev);

/**
 * Returns position resolution set during module initialization (2 * pi / ticks_per_revolution).
 */
double encrGetPositionResolution();

/**
 * Calculate value of current velocity basing on position and time change
 * @param dt time elapsed from last calculation [s]
 * @param encoderData encoder data structure containing currently registered position
 * @return calculated velocity [rad/s]
 */
double encrCalculateVelocity(double dt, encrData_t* encoderData);

#ifdef __cplusplus
}
#endif
#endif //WHEELDRIVERFIRMWARE_ENCR_H
