/// Error handling utilities

#ifndef WHEELDRIVERFIRMWARE_ERR_H
#define WHEELDRIVERFIRMWARE_ERR_H

#include <stdint.h>

#ifdef __cplusplus
extern "C"{
#endif

/**
 * Halts on error occurrence. Runs into infinite loop
 * @param moduleCode Code of module which error occurred in
 * @param errorCode Code of error type
 */
void errHalt(uint8_t moduleCode, uint8_t errorCode);

#ifdef __cplusplus
}
#endif

#endif //WHEELDRIVERFIRMWARE_ERR_H
