/** 
 * File containing config macros and definitions used by control system
 */

#ifndef WHEELDRIVERFIRMWARE_CONTROL_CONFIG_H
#define WHEELDRIVERFIRMWARE_CONTROL_CONFIG_H

#include "main.h"

#define W1_PWM &(TIM11->CCR1)
#define W1_ENC_TICK &(TIM1->CNT)

#define W2_PWM &(TIM10->CCR1)
#define W2_ENC_TICK &(TIM4->CNT)

#define W3_PWM &(TIM2->CCR1)
#define W3_ENC_TICK &(TIM3->CNT)

#define W4_PWM &(TIM2->CCR3)
#define W4_ENC_TICK &(TIM5->CNT)

#define CONTROL_LOOP_PERIOD_MS 20
#define SERIAL_COM_LOOP_PERIOD_MS 33 // 30Hz
#define DIODE_BLINK_PERIOD_MS 200 // 10Hz

#define ENCR_TICKS_PER_REVOLUTION 1320

#define WHEEL1_INDEX 0
#define WHEEL2_INDEX 1
#define WHEEL3_INDEX 2
#define WHEEL4_INDEX 3
#define NUMBER_OF_WHEELS 4

#define PID_P 6.842
#define PID_I 72.13
#define PID_D 0.
#define PID_CLAMPED true
#define PID_CLAMP_MAX 100.
#define PID_CLAMP_MIN 0.
#define PID_ENABLE true

#endif //WHEELDRIVERFIRMWARE_CONTROL_CONFIG_H
