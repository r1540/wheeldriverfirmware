/** @brief Motor control module for pwm steered DC motor drivers based on HAL library.
 * @remarks
 * Assumptions about motor driver inputs:
 *   - A, B ports for direction steering and hard/soft motor stopping
 *   - PWM input for motor effort
 */
#ifndef WHEELDRIVERFIRMWARE_MTPWM_H
#define WHEELDRIVERFIRMWARE_MTPWM_H

#include <stm32f4xx.h>

// region PUBLIC DEFINES ##################################

#define MTPWM_ERROR_CODE 2u ///< errHalt module code
#define MTPWM_ERROR_UNKNOWN_DIRECTION 1u ///< Raised in case of internal error of @ref mtpwmSetEffort
#define MTPWM_ERROR_UNKNOWN_STOP_TYPE 2u ///< Raised in case of calling @ref mtpwmStop with wrong direction parameter value
#define MTPWM_ERROR_DIRECTION_NOT_SET 3u ///< Raised in case of setting effort while, direction set is None
#define MTPWM_ERROR_OPERATING_DURING_STANDBY 4u ///< Raised in case of trying to operate while in Standby mode
#define MTPWM_ERROR_STANDBY_PIN_NOT_SET 5u ///< Raised in case of trying to set PowerOn mode without standby port set
#define MTPWM_ERROR_UNKNOWN_MODE 6u ///< Raised in case of trying to set mode other than PowerOn or Standby

#define EFFORT_MIN 0.
#define EFFORT_MAX 100.

// endregion ###############################################

#ifdef __cplusplus
extern "C"{
#endif

// region PUBLIC TYPES #####################################

/**
 * Defines single port information, where "port" means concrete GPIO (in output mode) pin.
 */
typedef struct {
    GPIO_TypeDef *const port; ///< HAL GPIO port (PORTA, PORTB, ...)
    const uint16_t pin;       ///< HAL GPIO pin (GPIO_PIN_0, GPIO_PIN_1, ..)
} mtpwmPort_t;

/**
 * Motor stopping types.
 */
typedef enum {
    SoftStop, ///< High impedance on output ports - motor stops softly.
    HardStop  ///< So called short brake - motor stops immediately.
} mtpwmStopType_t;

/**
 * Available motor rotation directions.
 */
typedef enum {
    None = 0,
    Forward,
    Backward
} mtpwmDirection_t;

/**
 * Mode of motor drivers
 */
typedef enum{
    Standby, ///< Drivers in low power mode, no ability to control motors
    PowerOn, ///< Drivers on, operable by mtpwm module
    NotInitialized //< Standby port not set, module in inconsistent state
} mtpwmMode_t;

/**
 * Holds A,B ports and pointer to control PWM output for a single motor.
 * For more details about motor control mechanism read description of this module.
 */
typedef struct {
    mtpwmPort_t portA, portB;                 ///< Port for motor direction control (see module description for details).
    volatile uint32_t *const wheelPWM;   ///< Pointer for PWM output control. Values such as &TIM1->CCR1 are expected.
    mtpwmDirection_t currentDirection;   ///< Motor rotation direction currently set
} mtpwmMotorControl_t;

// endregion ###############################################

// region PUBLIC FUNCTIONS #################################

/**
 * Sets motor effort by controlling PWM duty.
 * @param motor Motor structure pointer.
 * @param effort Value has to be between EFFORT_MAX and EFFORT_MIN (values outside the range are trimmed).
 * @throws MTPWM_ERROR_DIRECTION_NOT_SET if motor direction set is None
 * @throws MTPWM_ERROR_OPERATING_DURING_STANDBY if mode is set to Standby
 */
void mtpwmSetEffort(mtpwmMotorControl_t *motor, double effort);

/**
 * Immediately stops motor.
 * @param motor Motor structure pointer.
 * @param stopType Motor stop type (for details see @ref mtpwmStopType_t).
 * @throws MTPWM_ERROR_UNKNOWN_STOP_TYPE if stopType is value not present in enum
 */
void mtpwmStop(mtpwmMotorControl_t *motor, mtpwmStopType_t stopType);

/**
 * Set rotation direction of the motor, by altering A and B pin states.
 * @param motor controlled motor
 * @param direction requested direction rotation
 * @throws MTPWM_ERROR_UNKNOWN_DIRECTION if direction param is other than Forward or Backward
 * @throws MTPWM_ERROR_OPERATING_DURING_STANDBY if mode is set to Standby
 */
void mtpwmSetDirection(mtpwmMotorControl_t *motor, mtpwmDirection_t direction);

/**
 * @brief Set mode of drivers.
 * @throws MTPWM_ERROR_STANDBY_PIN_NOT_SET if standby port is not set (can be done with @ref mtpwmSetStandbyPort)
 */
void mtpwmSetMode(mtpwmMode_t mode);

/**
 * Get current mode of drivers
 */
mtpwmMode_t mtpwmGetMode();

/**
 * Set standby port
 */
void mtpwmSetStandbyPort(mtpwmPort_t port);

// endregion ###############################################

#ifdef __cplusplus
}
#endif

#endif //WHEELDRIVERFIRMWARE_MTPWM_H
