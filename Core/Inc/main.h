/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
/**
 * Start UART receiving
 * @return status of the operation
 */
HAL_StatusTypeDef startUART_receive();
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define TIMERS_CLOCK_FREQ 84000000
#define PWM_TIMERS_PRESCALER 0
#define PWM_TIMERS_ARR 999
#define USER_ACTIVITY_TIMEOUT_MS 500
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define B1_EXTI_IRQn EXTI15_10_IRQn
#define W4_E_A_Pin GPIO_PIN_0
#define W4_E_A_GPIO_Port GPIOA
#define W4_E_B_Pin GPIO_PIN_1
#define W4_E_B_GPIO_Port GPIOA
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define W3_E_A_Pin GPIO_PIN_6
#define W3_E_A_GPIO_Port GPIOA
#define W3_E_B_Pin GPIO_PIN_7
#define W3_E_B_GPIO_Port GPIOA
#define W4_PWM_Pin GPIO_PIN_10
#define W4_PWM_GPIO_Port GPIOB
#define W4_O_A_Pin GPIO_PIN_12
#define W4_O_A_GPIO_Port GPIOB
#define W4_O_B_Pin GPIO_PIN_13
#define W4_O_B_GPIO_Port GPIOB
#define W3_O_A_Pin GPIO_PIN_14
#define W3_O_A_GPIO_Port GPIOB
#define W3_O_B_Pin GPIO_PIN_15
#define W3_O_B_GPIO_Port GPIOB
#define W2_O_A_Pin GPIO_PIN_6
#define W2_O_A_GPIO_Port GPIOC
#define W2_O_B_Pin GPIO_PIN_7
#define W2_O_B_GPIO_Port GPIOC
#define W1_O_A_Pin GPIO_PIN_8
#define W1_O_A_GPIO_Port GPIOC
#define W1_O_B_Pin GPIO_PIN_9
#define W1_O_B_GPIO_Port GPIOC
#define W1_E_A_Pin GPIO_PIN_8
#define W1_E_A_GPIO_Port GPIOA
#define W1_E_B_Pin GPIO_PIN_9
#define W1_E_B_GPIO_Port GPIOA
#define DRV_NSTDBY_Pin GPIO_PIN_10
#define DRV_NSTDBY_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define W3_PWM_Pin GPIO_PIN_15
#define W3_PWM_GPIO_Port GPIOA
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define W2_E_A_Pin GPIO_PIN_6
#define W2_E_A_GPIO_Port GPIOB
#define W2_E_B_Pin GPIO_PIN_7
#define W2_E_B_GPIO_Port GPIOB
#define W2_PWM_Pin GPIO_PIN_8
#define W2_PWM_GPIO_Port GPIOB
#define W1_PWM_Pin GPIO_PIN_9
#define W1_PWM_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
_Static_assert(((size_t) USER_ACTIVITY_TIMEOUT_MS / 2) > 0, "USER_ACTIVITY_TIMEOUT_MS / 2 has to be higher than 0");
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
