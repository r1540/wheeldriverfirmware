/** 
 * Module containing necessary structs and functions for a single wheel control and data acquisition.
 * Initialize encr module before use.
 */

#ifndef WHEELDRIVERFIRMWARE_WCTRL_H
#define WHEELDRIVERFIRMWARE_WCTRL_H

#include "mtpwm.h"
#include "encr.h"
#include "pid.h"

#define WCTRL_ERROR_CODE 4U ///< errHalt module code
#define WCTRL_ERROR_STEP_DIRECTION_NONE 1U ///< raised in case of request of running control with no rotation direction set

#ifdef __cplusplus
extern "C"{
#endif

/// Struct used for wheel handling
typedef struct {
    double target;                          ///< Target speed in closed loop mode, target effort - otherwise
    double currentEffort;                   ///< Current effort set, mainly used for diagnosis
    mtpwmMotorControl_t motorController;    ///< Motor controlling struct
    encrData_t encoderData;                 ///< Encoder handling struct
    bool closedLoop;                        ///< Closed loop control flag
    pidPID_t pidController;                 ///< PID controller struct
} WheelController_t;

/**
 * Get current velocity of the wheel
 * @param wheelController WheelController struct of the wheel
 * @return Wheel velocity in [rad/s]
 */
double wctrlGetCurrentVelocity(const WheelController_t *wheelController);

/**
 * Get target velocity/effort of the wheel
 * @param wheelController WheelController struct of the wheel
 * @return Wheel target velocity in [rad/s] or effort
 */
double wctrlGetTarget(const WheelController_t *wheelController);

/**
 * Set target velocity/effort of the wheel proper rotation direction is set based on target sign.
 * In case of target == 0., hard stop is enabled and PID state is cleared
 * @param target Target velocity in [rad/s] or effort. For details see @ref WheelController_t doc.
 * @param wheelController WheelController struct of the wheel
 */
void wctrlSetTarget(double target, WheelController_t *wheelController);

/**
 * Perform control loop step of controller. In open loop mode, no PID processing will be done.
 * wheelController->currentEffort is set having regard rotation direction.
 * @param dt Time elapsed since last step [s]
 * @param wheelController WheelController struct of the wheel
 */
void wctrlStep(double dt, WheelController_t *wheelController);

/**
 * Hardly stops wheel, PID controller is disabled and internal state is cleared.
 * @param wheelController WheelController struct of the wheel
 */
void wctrlStop(WheelController_t *wheelController);

/**
 * Enables/disables close loop mode for particular wheel controller. In case of disabling controller state is cleared.
 * @param enable true - enables controller, false - controller is disabled and its state is cleared.
 * @param wheelController controller instance to be performed operation on
 */
void wctrlSetClosedLoop(bool enable, WheelController_t *wheelController);

#ifdef __cplusplus
}
#endif

#endif // WHEELDRIVERFIRMWARE_WCTRL_H
