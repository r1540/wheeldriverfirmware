#define __STM32F4xx_H // prevent inclusion of original stm32f4xx.h file in favor of stubbed one
#define __MAIN_H // same as above for main.h

#include <gtest/gtest.h>
#include "stubs/mtpwm/main.h"
#include "stubs/errHalt/errHalt.h"
#include "wctrl.h"

static mtpwmStopType_t mtpwmStopTypeCalled;
static double mtpwmEffortSet;
static double encrVelocityToBeSet;
static bool pidCleared;
static double pidOutputToBeSet;

extern "C" {
void mtpwmSetDirection(mtpwmMotorControl_t *motor, mtpwmDirection_t direction) {
    motor->currentDirection = direction;
}

void mtpwmStop(mtpwmMotorControl_t *motor, mtpwmStopType_t stopType) {
    motor->currentDirection = None;
    mtpwmStopTypeCalled = stopType;
}

void mtpwmSetEffort(mtpwmMotorControl_t *motor, double effort) {
    mtpwmEffortSet = effort;
}

double encrCalculateVelocity(double dt, encrData_t *encoderData) {
    encoderData->currentVelocity = encrVelocityToBeSet;
    return encrVelocityToBeSet;
}
}

class WctrlTest : public ::testing::Test {
protected:
    void SetUp() override {
        wheelController = new WheelController_t{
                .pidController{.p = 1., .i = 0., .d = 0},
        };
        mtpwmStopTypeCalled = (mtpwmStopType_t) 0xFF;
        mtpwmEffortSet = 0.;
        encrVelocityToBeSet = 0.;
        pidCleared = false;
        pidOutputToBeSet = 0.;
    }

    void TearDown() override {
        delete wheelController;
    }


    WheelController_t *wheelController = nullptr;
};

TEST_F(WctrlTest, DirectionNoneErrHaltCalledTest) {
    wheelController->closedLoop = true; // PID is enabled, but direction is not None - inconsistent state

    GTEST_EXPECT_ERRHALT(wctrlStep(1., wheelController),
                         WCTRL_ERROR_CODE,
                         WCTRL_ERROR_STEP_DIRECTION_NONE);
}

TEST_F(WctrlTest, ZeroTargetVelocityTest){
    wctrlSetTarget(0., wheelController);

    EXPECT_EQ(mtpwmStopTypeCalled, HardStop);
    EXPECT_EQ(wheelController->motorController.currentDirection, None);
}

TEST_F(WctrlTest, PositiveTargetTest) {
    encrVelocityToBeSet = 1.;
    wheelController->closedLoop = true;
    wctrlSetTarget(2., wheelController);

    EXPECT_EQ(wheelController->motorController.currentDirection, Forward);
    wctrlStep(1., wheelController);
    EXPECT_EQ(mtpwmEffortSet, 1.);
    EXPECT_EQ(wheelController->currentEffort, 1.);

    encrVelocityToBeSet = 3.;
    wctrlStep(1., wheelController);
    EXPECT_EQ(mtpwmEffortSet, -1.);
    EXPECT_EQ(wheelController->currentEffort, -1.);

}

TEST_F(WctrlTest, NegativeTargetTest){
    encrVelocityToBeSet = -1.;
    wheelController->closedLoop = true;
    wctrlSetTarget(-2., wheelController);

    EXPECT_EQ(wheelController->motorController.currentDirection, Backward);
    wctrlStep(1., wheelController);
    EXPECT_EQ(mtpwmEffortSet, 1.);
    EXPECT_EQ(wheelController->currentEffort, -1.);

    encrVelocityToBeSet = -3.;
    wctrlStep(1., wheelController);
    EXPECT_EQ(mtpwmEffortSet, -1.);
    EXPECT_EQ(wheelController->currentEffort, 1.);
}

TEST_F(WctrlTest, OpenLoopTest) {
    encrVelocityToBeSet = -1.;
    wctrlSetTarget(2., wheelController);

    EXPECT_EQ(wheelController->motorController.currentDirection, Forward);
    wctrlStep(50., wheelController);
    EXPECT_EQ(mtpwmEffortSet, 2.);
    EXPECT_EQ(wheelController->currentEffort, 2.);

    wctrlSetTarget(-50., wheelController);
    EXPECT_EQ(wheelController->motorController.currentDirection, Backward);
    wctrlStep(50., wheelController);
    EXPECT_EQ(mtpwmEffortSet, 50.);
    EXPECT_EQ(wheelController->currentEffort, -50.);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}