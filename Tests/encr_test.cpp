#include <gtest/gtest.h>
#include "encr.h"
#include "stubs/errHalt/errHalt.h"
#include <cmath>

class EncrTest : public ::testing::Test {

protected:
    void SetUp() override {
        encData = new encrData_t{.pCurrentPosition = &encRawPosition};
    }

    void TearDown() override {
        delete encData;
    }

    encrData_t *encData;
    uint32_t encRawPosition = 0u;
};

TEST_F(EncrTest, InitFailTest) {
    GTEST_EXPECT_ERRHALT(encrCalculateVelocity(1., encData), ENCR_ERROR_CODE, ENCR_ERROR_MODULE_NOT_INITIALIZED);
    GTEST_EXPECT_ERRHALT(encrInit(0), ENCR_ERROR_CODE, ENCR_ERROR_ZERO_TICKS_PER_REV);
}

TEST_F(EncrTest, InitTest) {
    encrInit(std::numeric_limits<uint32_t>::max());

    EXPECT_LT(encrGetPositionResolution(), 1e-8);
}

TEST_F(EncrTest, SimpleVelocityTest) {
    encrInit(5); // 1. for calculation simplicity

    double vel = encrCalculateVelocity(0.5, encData);
    EXPECT_FLOAT_EQ(vel, 0.); // no position change => velocity = 0

    encRawPosition = 5U;
    vel = encrCalculateVelocity(0.5, encData);
    EXPECT_FLOAT_EQ(vel, encData->currentVelocity);
    EXPECT_FLOAT_EQ(vel, 4 * M_PI); // (5-0) * 3 / 0.5 = 30
}

TEST_F(EncrTest, BoundaryValuesTest) {
    encrInit(1.);

    encRawPosition = 0xC001u;
    double vel = encrCalculateVelocity(2 * M_PI, encData);
    EXPECT_FLOAT_EQ(vel, -16'383.);

    encRawPosition = 0x1u;
    vel = encrCalculateVelocity(2 * M_PI, encData);
    EXPECT_FLOAT_EQ(vel, 16'384.);

    encRawPosition = 0x3FFFu;
    vel = encrCalculateVelocity(2 * M_PI, encData);
    EXPECT_FLOAT_EQ(vel, 16'382.);

    encRawPosition = 0x3FFEu;
    vel = encrCalculateVelocity(2 * M_PI, encData);
    EXPECT_FLOAT_EQ(vel, -1.);

    encRawPosition = 0x0u;
    vel = encrCalculateVelocity(2 * M_PI, encData);
    EXPECT_FLOAT_EQ(vel, -16'382.);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}