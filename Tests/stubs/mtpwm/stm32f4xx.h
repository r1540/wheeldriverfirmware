/// This file is used for stubbing HAL_GPIO module for unit testing purpose.
#ifndef WHEELDRIVERFIRMWARE_STM32F4XX_HAL_GPIO_H
#define WHEELDRIVERFIRMWARE_STM32F4XX_HAL_GPIO_H

#include "stdint.h"

#ifdef __cplusplus
extern "C"{
#endif

    /**
     * ATTENTION: this is stubbed struct
     * Original struct replaced with int for simplicity
     */
typedef int GPIO_TypeDef;

typedef enum
{
    GPIO_PIN_RESET = 0,
    GPIO_PIN_SET,
    NOT_INITIALIZED = 0xFF // additional value to test if anything has been written
}GPIO_PinState;

void HAL_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState);

#ifdef  __cplusplus
};
#endif

#endif //WHEELDRIVERFIRMWARE_STM32F4XX_HAL_GPIO_H
