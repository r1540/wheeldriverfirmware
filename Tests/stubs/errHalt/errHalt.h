/// In this file there are global variables used by stubbed errHalt function.

#ifndef WHEELDRIVERFIRMWARE_ERRHALT_H
#define WHEELDRIVERFIRMWARE_ERRHALT_H

#include <cstdint>
#include <exception>

#define GTEST_EXPECT_ERRHALT(statement, expectedModuleCode, expectedErrorCode)  \
    EXPECT_THROW({                                              \
    try{                                                        \
        statement;                                            \
    }                                                           \
    catch(const stubs::ErrHalt &e){                             \
        EXPECT_EQ(e.moduleCode(), expectedModuleCode);                \
        EXPECT_EQ(e.errorCode(), expectedErrorCode);                  \
        throw;                                                  \
    }                                                           \
    },                                                          \
    stubs::ErrHalt)

namespace stubs{
    class ErrHalt : public std::exception{
    public:
        ErrHalt(uint8_t moduleCode, uint8_t errorCode):
                m_errorCode(errorCode), m_moduleCode(moduleCode){}

        uint8_t moduleCode() const{ return m_moduleCode; }
        uint8_t errorCode() const{ return m_errorCode; }

    private:
        uint8_t m_moduleCode;
        uint8_t m_errorCode;
    };
}

#endif //WHEELDRIVERFIRMWARE_ERRHALT_H
