/// Includes definition of stubbed errHalt function

#include "errHalt.h"

#ifdef __cplusplus
extern "C" {
#endif


// stabbing errHalt from err.h module
void errHalt(uint8_t moduleCode, uint8_t errorCode) {
    throw stubs::ErrHalt(moduleCode, errorCode);
}

#ifdef __cplusplus
}
#endif