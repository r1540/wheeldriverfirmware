#include <gtest/gtest.h>
#include "pid.h"
#include "stubs/errHalt/errHalt.h"

constexpr double dMax = std::numeric_limits<double>::max();
constexpr double dMin = -std::numeric_limits<double>::max();

TEST(ClampTest, CorrectConfAndValueTest) {
    pidClampConf_t clampConf{.clamped = true,
            .clampMax = 20.,
            .clampMin = -10.
    };

    double val = 5.;

    pidClamp(&val, &clampConf);
    EXPECT_FLOAT_EQ(val, 5.);
}

TEST(ClampTest, CorrectConfValueOutOfRangeTest) {
    pidClampConf_t clampConf{.clamped = true,
            .clampMax = 20.,
            .clampMin = -10.
    };
    double val = dMax;

    pidClamp(&val, &clampConf);
    EXPECT_FLOAT_EQ(val, 20.);

    val = dMin;
    pidClamp(&val, &clampConf);
    EXPECT_FLOAT_EQ(val, -10.);
}

TEST(ClampTest, ClampingTurnedOffTest) {
    pidClampConf_t clampConf{.clamped = false,
            .clampMax = 20.,
            .clampMin = -10.
    };
    double val = dMax;

    pidClamp(&val, &clampConf);
    EXPECT_FLOAT_EQ(val, dMax);

    val = dMin;
    pidClamp(&val, &clampConf);
    EXPECT_FLOAT_EQ(val, dMin);
}

TEST(ClampTest, MinEqualMaxClampedErrorTest) {
    // Equal max and min caps
    pidClampConf_t clampConf = {.clamped = true,
            .clampMax = 0.,
            .clampMin = 0.
    };
    double val = 0.;

    GTEST_EXPECT_ERRHALT(pidClamp(&val, &clampConf),
                         PID_ERROR_CODE,
                         PID_ERROR_CLAMPING);
}

TEST(ClampTest, MinEqualMaxNotClampedNoErrorTest) {
    // Equal max and min caps
    pidClampConf_t clampConf = {.clamped = false,
            .clampMax = 0.,
            .clampMin = 0.
    };
    double val = 0.;

    pidClamp(&val, &clampConf);
}

TEST(ClampTest, MinHigherThanMaxTest) {
    // Equal max and min caps
    pidClampConf_t clampConf = {.clamped = true,
            .clampMax = -5.,
            .clampMin = 50.
    };
    double val = 0.;

    GTEST_EXPECT_ERRHALT(pidClamp(&val, &clampConf),
                         PID_ERROR_CODE,
                         PID_ERROR_CLAMPING);
}

TEST(PID_CoreTest, IntegrationNoClampTest){
    pidPID_t pidConf{.i = 10.};
    double ret;

    ret = pidIntegrate(2., 1., &pidConf);
    EXPECT_FLOAT_EQ(pidConf.iAcc, 20.); // 0 + 2 * 1 = 2
    EXPECT_FLOAT_EQ(ret, 20.);

    ret = pidIntegrate(-6., 2., &pidConf);
    EXPECT_FLOAT_EQ(pidConf.iAcc, -100.); // 2 + (-6) * 2 = -10
    EXPECT_FLOAT_EQ(ret, -100.);
}

TEST(PID_CoreTest, IntegrationClampedTest){
    pidPID_t pidConf = {.i = 5.,
                        .clamp = {.clamped = true,
                                  .clampMax = 7.,
                                  .clampMin = -3.}
    };
    double ret;

    ret = pidIntegrate(.5,2., &pidConf);
    EXPECT_FLOAT_EQ(pidConf.iAcc, 5.);
    EXPECT_FLOAT_EQ(ret, 5.);

    ret = pidIntegrate(dMax,
                 dMax,
                 &pidConf);
    EXPECT_FLOAT_EQ(pidConf.iAcc, 7.);
    EXPECT_FLOAT_EQ(ret, 7.);

    ret = pidIntegrate(-1., 1, &pidConf);
    EXPECT_FLOAT_EQ(pidConf.iAcc, 2.);
    EXPECT_FLOAT_EQ(ret, 2.);

    ret = pidIntegrate(dMin,
                 dMax,
                 &pidConf);
    EXPECT_FLOAT_EQ(pidConf.iAcc, -3.);
    EXPECT_FLOAT_EQ(ret, -3.);
}

TEST(PID_CoreTest, DifferentiationTest){
    pidPID_t pidConf{.d = 8.};
    double ret;

    ret = pidDifferentiate(20., 5., &pidConf);
    EXPECT_FLOAT_EQ(pidConf.dLast, 20.);
    EXPECT_FLOAT_EQ(ret, 32.); // ((20 - 0) / 5) * 8

    ret = pidDifferentiate(-30., 5., &pidConf);
    EXPECT_FLOAT_EQ(pidConf.dLast, -30.);
    EXPECT_FLOAT_EQ(ret, -80.); // ((-30 - 20) / 5) * 8
}

TEST(PID_CoreTest, OutputCalculationTest){
    pidPID_t  pidConf{.p = 3.,
                      .i = 5.,
                      .d = 6.,
                      .clamp = {.clamped = true,
                                .clampMax = 6.,
                                .clampMin = -6.},
                      };
    double ret;


    ret = pidCalculateOutput(1., 0., 1., &pidConf);
    EXPECT_FLOAT_EQ(ret, 14.);

    // setPoint achieved, only "I" works
    // calm "D"
    pidCalculateOutput(1., 1., 1., &pidConf);
    ret = pidCalculateOutput(1., 1., 1., &pidConf);
    EXPECT_FLOAT_EQ(ret, 5.);

    // check clamping
    // saturate "I"
    pidCalculateOutput(dMax, 1., dMax, &pidConf);
    // calm "D"
    pidCalculateOutput(1., 1., dMax, &pidConf);
    // "I" should be at its clamped value
    ret = pidCalculateOutput(1., 1., 1., &pidConf);
    EXPECT_FLOAT_EQ(ret, 6.);
}

TEST(PID_CoreTest, StateClearingTest){
    pidPID_t  pidConf{.p=1.,.i=1.,.d=1.};

    pidCalculateOutput(dMax, dMin, dMax, &pidConf);
    ASSERT_NE(pidConf.iAcc, 0.);
    ASSERT_NE(pidConf.dLast, 0.);

    pidClearPID_State(&pidConf);
    EXPECT_FLOAT_EQ(pidConf.iAcc, 0.);
    EXPECT_FLOAT_EQ(pidConf.dLast, 0.);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}