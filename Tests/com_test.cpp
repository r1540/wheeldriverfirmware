#include <gtest/gtest.h>
#include "com.h"

TEST(ComReadTest, GivenCorrectDataWhenSuccessIsReturnedThenProperValuesAreSet) {
    const char *msg = "1000,2000,3000,4000\n";
    size_t len = strlen(msg);

    comWheelSpeed_t readResult{};

    auto ret = comReadCommand((uint8_t *) msg, len, &readResult);

    ASSERT_EQ(ret, Success);

    EXPECT_FLOAT_EQ(readResult.speed[0], 1.);
    EXPECT_FLOAT_EQ(readResult.speed[1], 2.);
    EXPECT_FLOAT_EQ(readResult.speed[2], 3.);
    EXPECT_FLOAT_EQ(readResult.speed[3], 4.);
}

TEST(ComReadTest, GivenTooMuchTokensWhenReadIsCalledThenFailureIsReturned) {
    const char *msg = "1000,2000,3000,4000,5000\n";
    size_t len = strlen(msg);

    comWheelSpeed_t readResult{};

    auto ret = comReadCommand((uint8_t *) msg, len, &readResult);

    ASSERT_EQ(ret, InvalidPayload);
}

TEST(ComReadTest, GivenWrongCharactersTokensWhenFailureIsReturnedThenFieldsAreSet) {
    const char *msg = "1000,dsgdfvfd,3000,4000\n";
    size_t len = strlen(msg);

    comWheelSpeed_t readResult{};

    auto ret = comReadCommand((uint8_t *) msg, len, &readResult);

    ASSERT_EQ(ret, InvalidPayload);
}

TEST(ComReadTest, GivenTooLeastTokensWhenReadIsCalledThenSyntaxErrorIsReturned) {
    const char *msg = "1000,12\n";
    size_t len = strlen(msg);

    comWheelSpeed_t readResult{};

    auto ret = comReadCommand((uint8_t *) msg, len, &readResult);

    ASSERT_EQ(ret, SyntaxError);
}

TEST(ComReadTest, GivenEmptyMsgWhenReadIsCalledThenNeedMoreIsReturned) {
    const char *msg = "";
    size_t len = strlen(msg);

    comWheelSpeed_t readResult{};

    auto ret = comReadCommand((uint8_t *) msg, len, &readResult);

    ASSERT_EQ(ret, NeedMore);
}

TEST(ComSerializeTest, GivenCorrectSizeWhenSerializeIsCalledThenTrueIsReturned) {
    comWheelSpeed_t input{
            .speed {10., 20., 30, 40.}
    };
    const char *expectedMsg = "10000,20000,30000,40000\n";

    constexpr size_t SIZE = 256;
    char buf[SIZE] = {0};

    auto ret = comSerialize((uint8_t *) &buf[0], SIZE, &input);

    EXPECT_EQ(ret, strlen(expectedMsg));
    EXPECT_STREQ(buf, expectedMsg);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}