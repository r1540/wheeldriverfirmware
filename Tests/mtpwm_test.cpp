
#define __STM32F4xx_H // prevent inclusion of original stm32f4xx.h file in favor of stubbed one
#define __MAIN_H // same as above for main.h

#include "mtpwm.h"
#include "main.h"
#include <gtest/gtest.h>
#include "stubs/errHalt/errHalt.h"
#include <unordered_map>

// values used for initial state of GPIO_RegisteredState_t
#define GPIO_PIN_INITIAL_REGISTER 0xFFFF

/*
 * GLOSSARY
 * GPIO port - port in terms of GPIO, virtual port including many gpio pins
 * GPIO pin - number of pin in GPIO port
 * port - particular pin on particular GPIO port
 */

/// struct used for tracking value written to particular port
typedef struct {
    uint16_t gpioWrittenPin;      ///< GPIO pin which state was written to
    GPIO_PinState gpioPinState;   ///< Written state
} GPIO_RegisteredState_t;

// it's assumed that only 2 pins will be written, otherwise values will be overwritten
std::unordered_map<GPIO_TypeDef, GPIO_RegisteredState_t> gpioRegisteredStates;

extern "C" {

void HAL_GPIO_WritePin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState) {
    gpioRegisteredStates[*GPIOx].gpioWrittenPin = GPIO_Pin;
    gpioRegisteredStates[*GPIOx].gpioPinState = PinState;
}

}

class MtpwmTest : public ::testing::Test {

protected:
    static void SetUpTestSuite(){
        GTEST_EXPECT_ERRHALT(mtpwmSetMode(Standby), MTPWM_ERROR_CODE, MTPWM_ERROR_STANDBY_PIN_NOT_SET);
    }

    void SetUp() override {
        mtpwmSetStandbyPort({.port = &gpioPortStandby, .pin = gpioPinStandby});
        EXPECT_EQ(mtpwmGetMode(), Standby);
        EXPECT_NO_THROW(mtpwmSetMode(Standby));

        mtpwmSetMode(PowerOn);
        EXPECT_EQ(mtpwmGetMode(), PowerOn);

        setMotorControl();
        initGPIO_PortStates();
    }

    void TearDown() override {
        delete motorControl;
    }

    mtpwmMotorControl_t *motorControl;
    GPIO_TypeDef gpioPortA = 0xA;
    GPIO_TypeDef gpioPortB = 0xB;
    GPIO_TypeDef gpioPortStandby = 0xF;
    static constexpr uint16_t gpioPinA = 1;
    static constexpr uint16_t gpioPinB = 2;
    static constexpr uint16_t gpioPinStandby = 0xFF;
    static constexpr uint32_t pwmInitialValue = 0xFFFFFFFF;
    uint32_t writtenPWM;

private:
    void setMotorControl() {
        writtenPWM = pwmInitialValue;
        motorControl = new mtpwmMotorControl_t{
                .portA={.port=&gpioPortA, .pin=gpioPinA},
                .portB={.port=&gpioPortB, .pin=gpioPinB},
                .wheelPWM=&writtenPWM,
                .currentDirection = None
        };
    }

    void initGPIO_PortStates() {
        const GPIO_RegisteredState_t initialGpioPortState = {
                .gpioWrittenPin = GPIO_PIN_INITIAL_REGISTER,
                .gpioPinState = NOT_INITIALIZED
        };
        gpioRegisteredStates = {
                {gpioPortA, initialGpioPortState},
                {gpioPortB, initialGpioPortState},
                {gpioPortStandby, initialGpioPortState},
        };
    }

};

TEST_F(MtpwmTest, ThrowOnOperationTryWhileInStandbyTest){
    mtpwmSetMode(Standby);

    GTEST_EXPECT_ERRHALT(mtpwmSetEffort(motorControl,0.),
                         MTPWM_ERROR_CODE,
                         MTPWM_ERROR_OPERATING_DURING_STANDBY);
    GTEST_EXPECT_ERRHALT(mtpwmSetDirection(motorControl,Forward),
                         MTPWM_ERROR_CODE,
                         MTPWM_ERROR_OPERATING_DURING_STANDBY);
}

TEST_F(MtpwmTest, ThrowOnSetWrongModeTest){
    GTEST_EXPECT_ERRHALT(mtpwmSetMode(NotInitialized),
                         MTPWM_ERROR_CODE,
                         MTPWM_ERROR_UNKNOWN_MODE);
}

TEST_F(MtpwmTest, StandbyPinResetOnStandbyTest){
    mtpwmSetMode(Standby);
    EXPECT_EQ(gpioRegisteredStates[gpioPortStandby].gpioWrittenPin, gpioPinStandby);
    EXPECT_EQ(gpioRegisteredStates[gpioPortStandby].gpioPinState, GPIO_PIN_RESET);
}

TEST_F(MtpwmTest, StandbyPinSetOnPowerOnTest){
    mtpwmSetMode(PowerOn);
    EXPECT_EQ(gpioRegisteredStates[gpioPortStandby].gpioWrittenPin, gpioPinStandby);
    EXPECT_EQ(gpioRegisteredStates[gpioPortStandby].gpioPinState, GPIO_PIN_SET);
}

TEST_F(MtpwmTest, ThrowOnDirectionNotSetTest){
    GTEST_EXPECT_ERRHALT(mtpwmSetEffort(motorControl, 0.), MTPWM_ERROR_CODE, MTPWM_ERROR_DIRECTION_NOT_SET);
}

TEST_F(MtpwmTest, DirectionSettingTest){
    mtpwmSetDirection(motorControl, Forward);
    EXPECT_EQ(motorControl->currentDirection, Forward);

    EXPECT_EQ(gpioRegisteredStates[gpioPortA].gpioWrittenPin, gpioPinA);
    EXPECT_EQ(gpioRegisteredStates[gpioPortA].gpioPinState, GPIO_PIN_SET);

    EXPECT_EQ(gpioRegisteredStates[gpioPortB].gpioWrittenPin, gpioPinB);
    EXPECT_EQ(gpioRegisteredStates[gpioPortB].gpioPinState, GPIO_PIN_RESET);

    mtpwmSetDirection(motorControl, Backward);
    EXPECT_EQ(motorControl->currentDirection, Backward);

    EXPECT_EQ(gpioRegisteredStates[gpioPortA].gpioWrittenPin, gpioPinA);
    EXPECT_EQ(gpioRegisteredStates[gpioPortA].gpioPinState, GPIO_PIN_RESET);

    EXPECT_EQ(gpioRegisteredStates[gpioPortB].gpioWrittenPin, gpioPinB);
    EXPECT_EQ(gpioRegisteredStates[gpioPortB].gpioPinState, GPIO_PIN_SET);
}

TEST_F(MtpwmTest, EffortEdgeCasesTest) {
    mtpwmSetDirection(motorControl, Forward);
    mtpwmSetEffort(motorControl, 100.);

    EXPECT_EQ(writtenPWM, PWM_TIMERS_ARR);

    // check capping
    mtpwmSetEffort(motorControl, 101.);

    EXPECT_EQ(writtenPWM, PWM_TIMERS_ARR);

    mtpwmSetEffort(motorControl, -100.);

    EXPECT_EQ(writtenPWM, 0);

    mtpwmSetEffort(motorControl, 0.);

    EXPECT_EQ(writtenPWM, 0);

}

TEST_F(MtpwmTest, SoftStopTest) {
    mtpwmSetDirection(motorControl, Forward);
    mtpwmStop(motorControl, SoftStop);

    EXPECT_EQ(gpioRegisteredStates[gpioPortA].gpioWrittenPin, gpioPinA);
    EXPECT_EQ(gpioRegisteredStates[gpioPortA].gpioPinState, GPIO_PIN_RESET);

    EXPECT_EQ(gpioRegisteredStates[gpioPortB].gpioWrittenPin, gpioPinB);
    EXPECT_EQ(gpioRegisteredStates[gpioPortB].gpioPinState, GPIO_PIN_RESET);

    EXPECT_EQ(motorControl->currentDirection, None);

    EXPECT_EQ(writtenPWM, 0);
}

TEST_F(MtpwmTest, HardStopTest) {
    mtpwmSetDirection(motorControl, Forward);
    mtpwmStop(motorControl, HardStop);

    EXPECT_EQ(gpioRegisteredStates[gpioPortA].gpioWrittenPin, gpioPinA);
    EXPECT_EQ(gpioRegisteredStates[gpioPortA].gpioPinState, GPIO_PIN_SET);

    EXPECT_EQ(gpioRegisteredStates[gpioPortB].gpioWrittenPin, gpioPinB);
    EXPECT_EQ(gpioRegisteredStates[gpioPortB].gpioPinState, GPIO_PIN_SET);

    EXPECT_EQ(motorControl->currentDirection, None);

    EXPECT_EQ(writtenPWM, 0);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}