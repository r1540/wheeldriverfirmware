# WheelDriverFirmware v0.1

Source code for RoboCoon's wheel driver.
- Target binary:
  - Platform: STM32 NUCLEO-F411RE (STM32F411RET6)
  - Languages: C, C++
  - Frameworks: FreeRTOS, GTest
- Unit tests:
  - Languages: C, C++
  - Frameworks: GTest

## Installation

TODO

## Usage

### Build

Project has two parallel configurations: cross-compile and testing. 
The first simply builds target binary and hex files (uses arm-none-eabi-compiler).
The latter builds unit tests (with gtest, using native host compiler).
To switch between the two options use `BUILD_TESTS` CMake option.

## License

This project uses BSD 3-Clause license.
